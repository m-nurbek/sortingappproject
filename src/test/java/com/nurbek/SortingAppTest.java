package com.nurbek;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class SortingAppTest {
    private SortingApp sortingApp;

    @Before
    public void initialize() {
        sortingApp = new SortingApp();
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private String runMethod(String... args) {
        return Arrays.stream(sortingApp.sort(args)).reduce((a, b) -> a + " " + b).orElse("");
    }

    @Test
    @FileParameters("src/test/resources/params.csv")
    public void testArgumentsInBounds(String input, String expected) {
        String output = runMethod(input.trim().split(" "));
        assertThat(output, containsString(expected));
    }

    @Test
    public void testZeroArguments() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(containsString("Illegal number of arguments"));
        runMethod();
    }

    @Test
    public void testOneArgument() {
        String output = runMethod("5");
        String expected = "5";

        assertThat(output, containsString(expected));
    }

    @Test
    public void testTenArguments() {
        String output = runMethod("5", "3", "7", "1", "9", "4", "6", "2", "8", "0");
        String expected = "0 1 2 3 4 5 6 7 8 9";
        assertThat(output, containsString(expected));
    }

    @Test
    public void testMoreThanTenArguments() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(containsString("Illegal number of arguments"));
        runMethod("5", "3", "7", "1", "9", "4", "6", "2", "8", "0", "10");
    }

    @Test
    public void testWrongArgumentsFormat() {
        exceptionRule.expect(NumberFormatException.class);
        exceptionRule.expectMessage("Wrong arguments format");
        runMethod("", "2", "df");
    }
}