package com.nurbek;


import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SortingApp {
    private static final Logger LOG = Logger.getLogger(SortingApp.class.getName());

    public String[] sort(String[] args) {
        String[] sorted = {};
        if (args.length == 0 || args.length > 10) {
            LOG.log(Level.WARNING, "Number of arguments is out of bounds!");
            throw new IllegalArgumentException("Illegal number of arguments");
        }

        // Convert the args to ints
        int[] numbers = new int[args.length];
        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            LOG.log(Level.WARNING, "Please enter valid integer values");
            throw new NumberFormatException("Wrong arguments format");
        }

        // Sort
        Arrays.sort(numbers);

        // Print the sorted result
        StringBuilder result = new StringBuilder();
        for (int number : numbers) {
            result.append(number).append(" ");
        }
        LOG.log(Level.INFO, result.toString().trim());

        return result.toString().trim().split(" ");
    }

    public static void main(String[] args) {
        SortingApp sortingApp = new SortingApp();
        System.out.println(Arrays.toString(sortingApp.sort(args)));
    }
}